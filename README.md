# Dnds

This tool takes as input a directory of protein families (fasta files) and computes the dN/dS ratio for each family

# Nota Bene

* The family names will be defined according to the filenames of the fasta files found inside the input directory
* This tool requires the headers of the sequences within the fasta files to begin with an NCBI protein ID (e.g. >AMQ45836.1 .*)


# Usage

```
usage: dnds.py [-h] -i INPUT_DIRECTORY -o OUTPUT_DIRECTORY
               [--fs FIELD_SEPARATOR] [--aligner ALIGNER] --snap SNAP
               [--efetch EFETCH]

Arguments:
  -h, --help            show this help message and exit
  -i INPUT_DIRECTORY    path to the directory of protein families
  -o OUTPUT_DIRECTORY   path to the output directory
  --fs FIELD_SEPARATOR  as defined by this field separator, the first field in
                        the header of each protein sequence within  will be considered
                        as the NCBI protein ID (default = " ")
  --aligner ALIGNER     name of the binary to be called for the multiple
                        sequence alignments (default = muscle)
  --snap SNAP           path to the SNAP perl script
  --efetch EFETCH       path to the efetch E-Utility (default = efetch)
```
  
# Usage example

```bash
python dnds.py -i /home/user/my_families -o /home/user/dnds_output --fs ' ' --aligner muscle --snap /opt/SNAP/SNAP.pl --efetch /opt/edirect/efetch
```

# Dependencies

* python3
* Muscle (https://www.drive5.com/muscle/downloads.htm) or Mafft (https://mafft.cbrc.jp/alignment/software/)
* TrimAl (http://trimal.cgenomics.org/downloads)
* SNAP (https://www.hiv.lanl.gov/repository/aids-db/PROGS/Snap)
* efetch (https://www.ncbi.nlm.nih.gov/books/NBK179288/bin/install-edirect.sh)