# -*- coding: utf-8 -*-
"""
Created on Fri Mar 29 15:25:45 2019

@author: charles
"""
import argparse
import multiprocessing
import os
import re
import subprocess
import sys


# CREATE THE OUTPUT DIRECTORY IF IT DOES NOT EXIST YET
def make_outdir(output_directory):
    # Create the output directory if it does not exist and its parent directory does
    if not os.path.exists(output_directory):
        parent_dir = os.path.dirname(output_directory)
        if os.path.exists(parent_dir):
            os.mkdir(output_directory)
        else:
            sys.exit('Unable to locate \'%s\'. Please provide a correct path to the output directory' % parent_dir)
    # Return error if the output directory is not empty
    else:
        if os.listdir(output_directory):
            sys.exit('The directory \'%s\' is not empty. Please free it or provide another output directory' % output_directory)


# GET EACH PROTEIN FAMILY FROM THE INPUT DIRECTORY
def get_families(input_directory):
    print('Retrieving protein families from the input directory ...')
    fam_dict = dict()
    for filename in os.listdir(input_directory):
        filepath = os.path.join(input_directory, filename)
        # Use regex to retrieve only the fasta files within the input directory
        if re.search(r'.*\.fa(a|sta)?$', filename):
            # Check that 1st line of the current fasta begins with the '>' character
            with open(filepath, mode = 'r') as file:
                line = file.readline()
            file.close()
            if not re.match(r'^>', line):
                sys.exit('The fasta \'%s\' does not begin with the expected ">" character' % filepath)
            # Append to the dictionary of protein families
            family_name = os.path.splitext(filename)[0]
            fam_dict[family_name] = dict()
            fam_dict[family_name]['faa'] = filepath
    print(' * %d protein families retrieved ...' % len(fam_dict))
    return(fam_dict)
    

# GET IDS OF ALL THE SEQUENCES IN THE FASTA OF EACH PROTEIN FAMILY
def get_ids(fam_dict, fs):
    print('Retrieving NCBI protein IDs within each family ...') 
    families = [*fam_dict]
    n = 0;
    for fam in families:
        n = n + 1
        list_ids = list()
        with open(fam_dict[fam]['faa'], mode = 'r') as file:
            for line in file:
                # Get the NCBI protein id (the first field of the current sequence header)
                if line[0] == '>':
                    list_ids.append(line[1:].split(fs)[0])
        file.close()
        fam_dict[fam]['ids'] = list_ids
    return fam_dict


# FETCH CDS (ON THE NCBI) CORRESPONDING TO THE PROTEIN IDS
# Cannot be parallelized due to NCBI download/second limitations
def fetch_cds(efetch_path, fam_dict, output_directory):
    print('Fetching the corresponding coding sequence (CDS) for each protein within the families')
    cds_directory = os.path.join(output_directory, 'CDS')
    os.mkdir(cds_directory)
    families = [*fam_dict]
    n = 0
    for fam in families:
        n = n + 1
        print(' * processing family \'%s\' (%d/%d) ...' % (fam, n, len(fam_dict)))
        fam_dict[fam]['fna'] = os.path.join(cds_directory, fam + '.fna')
        with open(fam_dict[fam]['fna'], 'w+') as outfile:
            for protein_id in fam_dict[fam]['ids']:
                # Use E-entrez utilities to fetch the cds sequence corresponding to the current protein id 
                process = subprocess.Popen([efetch_path, '-db', 'protein', '-format', 'fasta_cds_na', '-id', protein_id], 
                                           stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
                stdout, error = process.communicate()
                if error:
                    print('WARNING: unable to fetch the CDS for the \'%s\' protein ID from the \'%s\' file' % (protein_id, fam_dict[fam]['faa']))
                    # print('WARNING: %s' % error)
                else:
                    # Replace CDS header by the ID of the corresponding protein
                    outfile.write('>%s\n' % protein_id)
                    outfile.write('\n'.join(stdout.split('\n')[1:]))
        outfile.close()
    return fam_dict


# MAKE MSA FOR EACH PROTEIN FAMILY
# This is the function that will be parallelized
def run_msa(fam, aligner, in_filename, out_filename):
    if(aligner == 'muscle'):
        process = subprocess.Popen([aligner, '-maxiters', '50', '-in', in_filename, '-out', out_filename], 
                                    stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
        stdout, error = process.communicate()
        # unfortunately, the stdout of muscle goes into the error channel so stdout cannot be distinguish from real error
    elif(aligner == 'mafft'):
        outfile = open(out_filename, mode = 'w+')
        process = subprocess.Popen([aligner, '--maxiterate', '1000', in_filename],
                                   stdout=outfile, stderr=subprocess.PIPE, universal_newlines=True)
        stdout, error = process.communicate()
        outfile.close()
        if error:
            print('WARNING: for the \'%s\' family, mafft exited with following error: %s' % (fam, error))
    else:
        sys.exit('Only mafft or muscle are accepted as aligners')
    print(' * Finished processing the \'%s\' family ...' % fam)
    
# This is the function that defines and launches the processes to be parallelized                   
def msa(aligner, fam_dict, output_directory):
    print('Parallelizing Multiple Sequence Alignment (MSA) of the protein families ...')
    msa_directory = os.path.join(output_directory, 'proteins_MSA')
    os.mkdir(msa_directory)
    families = [*fam_dict]
    # Use parallel processing to speed up the job
    processes = list()
    for fam in families:
        fam_dict[fam]['msa'] = os.path.join(msa_directory, fam + '_MSA.fasta')
        task = multiprocessing.Process(target = run_msa, 
                                       args = (fam, aligner, fam_dict[fam]['faa'], fam_dict[fam]['msa']))
        processes.append(task)
        task.start()
    for process in processes:
        process.join()
    print('Parallel computing done!')
    return fam_dict
        

# TRIM MSA OF EACH PROTEIN FAMILY AND KEEP TRACK OF WHICH RESIDUE POSITIONS OF THE ORIGINAL ALIGNMENT HAVE BEEN RETAINED
def trim(fam_dict, output_directory):
    print('Trimming the MSA of each family ...')
    trim_directory = os.path.join(output_directory, 'proteins_MSA_trimmed')
    os.mkdir(trim_directory)
    families = [*fam_dict]
    for fam in families:
        fam_dict[fam]['msa_trimmed'] = os.path.join(trim_directory, fam + '_MSA_trimmed.fasta')
        fam_dict[fam]['positions_after_trimal'] = os.path.join(trim_directory, fam + '_remaining_residues_after_trimming.txt')
        # the -colnumbering option prints in stdout the positions remaining after trimming 
        with open(fam_dict[fam]['positions_after_trimal'], 'w+') as positions_file:
            process = subprocess.Popen(['trimal', '-gt', '0.8', '-colnumbering', '-in', fam_dict[fam]['msa'], '-out', fam_dict[fam]['msa_trimmed']],
                                       stdout=positions_file, stderr=subprocess.PIPE, universal_newlines = True)
            _, error = process.communicate()
        if error:
            print('WARNING: for the \'%s\' family, trimal exited with the following error: %s' % (fam, error))
        positions_file.close()
    return fam_dict


# CONVERT TRIMMED PROTEIN MSA TO CORRESPONDING CDS MSA FOR EACH PROTEIN FAMILY
# This is the core function of the step 2 (see below). 
# Return a correspondence between position of the aa in the MSA and position of the same aa in the raw sequence.
# -AW-Y (5 first residues of the entry in the msa)
# AWYMG (5 first residues in the protein sequence)
# position_converter[(0,1,2,3,4)] = (-1,0,1,-1,2) # with -1 meaning gap
def msa_pos_to_seq_pos(msa_sequence):
    position_converter = dict()
    pos_seq = 0
    for pos_msa in range(0, len(msa_sequence)):
        # if the position in the MSA is not a gap, then iterate the "pos_seq" counter 
        if(msa_sequence[pos_msa] != '-'):
            position_converter[pos_msa] = pos_seq
            pos_seq = pos_seq + 1
        else:
            # A position of -1 means that "---" should be further used as the codon string
            position_converter[pos_msa] = -1
    return position_converter


# This is the core function of the step 3 (see below)
# Return the corresponding cds string given a list of amino acid positions
def aa_to_codon(retained_positions, position_converter, cds_sequence):
    cds_msa_sequence = ''
    for i in range(0, len(retained_positions)):
        # start position of a codon = 3 * start position of the corresponding amino-acid
        start = position_converter[int(retained_positions[i])]*3
        if start < 0:
            codon = '---'
        else:
            codon = cds_sequence[start:(start+3)]
        cds_msa_sequence = cds_msa_sequence + codon
    return cds_msa_sequence
    

def prot_msa_to_cds_msa(fam_dict, output_directory, fs):
    print('Converting protein MSA to CDS MSA for each family ...')
    cds_msa_directory = os.path.join(output_directory, 'CDS_MSA')
    os.mkdir(cds_msa_directory)
    families = [*fam_dict]
    for fam in families:
        fam_dict[fam]['cds_msa'] = os.path.join(cds_msa_directory, fam + '_CDS_MSA.tsv')
        # STEP 1: Get the positions of all the residues of the MSA that have been retained after trimming
        with open(fam_dict[fam]['positions_after_trimal'], 'r') as file:
            empty_header = file.readline()
            position_string = file.readline().strip()
            retained_positions = position_string.split(', ')
        file.close()
        # STEP 2: Link position of all the residues in the complete MSA (gaps included)
        # and corresponding positions in each input protein sequence (gaps excluded).
        real_position = dict()
        curr_msa_sequence = curr_id = ''
        with open(fam_dict[fam]['msa'], 'r') as file:
            for line in file:
                if(line[0] == '>'):
                    # Process previous sequence
                    if curr_msa_sequence:
                        real_position[curr_id] = msa_pos_to_seq_pos(curr_msa_sequence)
                    # Initialize new sequence and retrieve its ID 
                    curr_msa_sequence = ''
                    curr_id = line[1:].split(fs)[0]
                else:
                    # Retrieve new sequence 
                    curr_msa_sequence = curr_msa_sequence + line.strip()
            # Don't forget to process last sequence as well
            real_position[curr_id] = msa_pos_to_seq_pos(curr_msa_sequence)        
        file.close()
        # STEP 3: Create MSA of CDS using the positions of the amino-acids that must retained
        curr_cds_sequence = curr_id = ''
        with open(fam_dict[fam]['cds_msa'], 'w+') as outfile:
            with open(fam_dict[fam]['fna'], 'r') as infile:
                for line in infile:
                    if(line[0] == '>'):
                        if curr_cds_sequence:
                            curr_cds_msa_sequence = aa_to_codon(retained_positions, real_position[curr_id], curr_cds_sequence)
                            # This is format of the MSA that SNAP accepts as input
                            outfile.write('>%s\t%s\n' % (curr_id, curr_cds_msa_sequence))
                        # Initialize new sequence and retrieve its ID 
                        curr_cds_sequence = ''
                        curr_id = line[1:].strip()
                    else:
                        curr_cds_sequence = curr_cds_sequence + line.strip()
                # Don't forget to process last sequence as well
                curr_cds_msa_sequence = aa_to_codon(retained_positions, real_position[curr_id], curr_cds_sequence)
                outfile.write('>%s\t%s\n' % (curr_id, curr_cds_msa_sequence))
            infile.close()
        outfile.close()
    return fam_dict


# COMPUTE THE dN/dS RATIO FROM THE CDS MSA OF EACH FAMILY
def snap(fam, snap_path, cds_msa, dnds_dir):
    # Snap produces its output files in the current directory './' 
    os.chdir(dnds_dir)
    process = subprocess.Popen(['perl', snap_path, cds_msa], 
                               stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
    stdout, error = process.communicate()
    if error:
        print('WARNING: for the \'%s\' family, SNAP exited with the following error: %s' % (fam, error))
    print(' * Finished processing the \'%s\' family ...' % fam)
    os.chdir('../')
        
    
def dnds(snap_path, fam_dict, output_directory):
    print('Parallelizing computation of the dN/dS ratio of each family ...')
    initial_dir = os.getcwd()
    dnds_directory = os.path.join(output_directory, 'dnds')
    os.mkdir(dnds_directory)
    families = [*fam_dict]
    # use parallel processing to speed up the job
    processes = list()
    for fam in families:
        fam_dict[fam]['dnds_dir'] = os.path.join(dnds_directory, fam + '_dnds')
        os.mkdir(fam_dict[fam]['dnds_dir'])
        task = multiprocessing.Process(target = snap,
                                       args = (fam, snap_path, fam_dict[fam]['cds_msa'], fam_dict[fam]['dnds_dir']))
        processes.append(task)
        task.start()
    for process in processes:
        process.join()
    print('Parallel computing done!')
    os.chdir(initial_dir)
    return fam_dict
    

# MAKE A FINAL TABULAR FILE THAT ASSIGNS A DNDS RATIO TO EACH FAMILY
def summarize(fam_dict, output_directory):
    print('Creating a summary file that assigns a dN/dS ratio to each family ...')
    families = [*fam_dict]
    with open(os.path.join(output_directory, 'summary.tsv'), mode = 'w+') as outfile:
        outfile.write('family\tdN/dS\n')
        for fam in families:
            dnds_value = 0
            for filename in os.listdir(fam_dict[fam]['dnds_dir']):
                # The dS/dN is given by a specific line in the summary* file
                # The dN/dS is then just the invert of the dS/dN
                if re.search('^summary', filename):
                    with open(os.path.join(fam_dict[fam]['dnds_dir'], filename), mode = 'r') as file:
                        for line in file:
                            if(line[0:36] == 'Averages of all pairwise comparisons'):
                                dsdn_field = line.strip().split(', ')[2]
                                dsdn_value = float(dsdn_field.split(' =  ')[1])
                                dnds_value = 1 / dsdn_value
                    file.close()
            outfile.write('%s\t%.3f\n' % (fam, dnds_value))
    outfile.close()


def __main__():
    # Read arguments
    parser = argparse.ArgumentParser(description='This tool takes as input a directory of protein families (fastas with NCBI IDs) and computes the dN/dS ratio for each family')
    parser.add_argument('-i', dest='input_directory', type=str, required=True,
                        help='path to the directory of protein families')
    parser.add_argument('-o', dest='output_directory', type=str, required=True,
                        help='path to the output directory')
    parser.add_argument('--fs', dest='field_separator', type=str, required=False, default=' ', 
                        help='as defined by this field separator, the first field in the header of each protein sequence will be considered as the NCBI protein ID (default = " ")')
    parser.add_argument('--aligner', dest='aligner', type=str, required=False, default='muscle', 
                        help='name of the binary to be called for the multiple sequence alignments (default = muscle)')
    parser.add_argument('--snap', dest='snap', type=str, required=True,
                        help='path to the SNAP perl script')
    parser.add_argument('--efetch', dest='efetch', type=str, required=False, default='efetch', 
                        help='path to the efetch E-entrez utility (default = efetch)')
    args = parser.parse_args()
    
    args.output_directory = os.path.abspath(args.output_directory)
    args.snap = os.path.abspath(args.snap)
    
    make_outdir(args.output_directory)
    fam_dict = get_families(args.input_directory)
    fam_dict = get_ids(fam_dict, args.field_separator)
    fam_dict = fetch_cds(args.efetch, fam_dict, args.output_directory)
    fam_dict = msa(args.aligner, fam_dict, args.output_directory)
    fam_dict = trim(fam_dict, args.output_directory)
    fam_dict = prot_msa_to_cds_msa(fam_dict, args.output_directory, args.field_separator)
    fam_dict = dnds(args.snap, fam_dict, args.output_directory)
    summarize(fam_dict, args.output_directory)
    print('Done!')


if __name__=='__main__': __main__()
